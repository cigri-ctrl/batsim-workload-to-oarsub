{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/22.11";
  };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in
    {

      packages.${system} = rec {
        default = batsim_to_oarsub;
        batsim_to_oarsub = pkgs.python3Packages.buildPythonPackage {
          pname = "batsim_to_oarsub";
          version = "0.0";
          src = ./.;
          propagatedBuildInputs = [ ];
          doCheck = false;
        };
      };

      nixosModules = rec {
        default = batsim_to_oarsub;
        batsim_to_oarsub = import ./module.nix { inherit self; };
      };
      
      overlay = import ./overlay.nix { inherit self; };
    };
}
