{self}:
{ config, lib }:

with lib;

let
  cfg = config.services.batsim-to-oarsub;
in
{

  options = {
    services.batsim-to-oarsub = {

      enable = mkEnableOption "Batsim to oarsub";

      workload = mkOption {
        type = types.path;
        description = "Batsim workload";
      };

      csv_file = mkOption {
        type = types.str;
        description = "Path to CSV file to store the result";
      };

      package = mkOption {
        type = types.package;
        default = pkgs.batsim-to-oarsub;
      };
    };
  };

  config = mkIf (cfg.enable) {
    systemd.services.batsim-to-oarsub = {
      description = "Batsim to oarsub";
      after = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart = "${cfg.package}/bin/batsim-to-oarsub --workload ${cfg.workload} --csv_file ${cfg.csv_file}";
        KillMode = "process";
        Restart = "on-failure";
      };
    };
  };
}
