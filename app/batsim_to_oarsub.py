import argparse
import sched
import csv
import time
import json
import re
from subprocess import call, Popen, PIPE


def get_profiles(json_data):
    return json_data["profiles"] 
    
def get_jobs(json_data):
    return json_data["jobs"]
        
def oar_command(job, profiles, writer, debug=False):
    nb_nodes = job["res"]
    walltime = job["walltime"]
    runtime = profiles[job["profile"]]["runtime"]
    oar_cmd = f"oarsub -l resource_id={nb_nodes},walltime={walltime} \"sleep {runtime}\""
    if debug:
        print(oar_cmd)
    else:
        with Popen(oar_cmd, stdout=PIPE, shell=True) as oar_cmd:
            stdout = oar_cmd.stdout.read().decode()
            find_job = re.search("\\nOAR_JOB_ID=(.*)\\n", stdout)
            if find_job:
                job_id = int(find_job.group(1))
                writer.writerow({"batsim_id": job["id"], "oar_id": job_id})
            else:
                print("JOB_NOT_SUBMITTED_BATSIM_JOB_ID: {}".format(job["id"]))

def main():
    parser = argparse.ArgumentParser(description='Batsim workload to OAR submitions')
    parser.add_argument('--workload', help='Batsim workload')
    parser.add_argument('--csv_file', help='CSV file storing results')
    parser.add_argument('--debug', action='store_true', help='debug')

    args = parser.parse_args()

    oarsub_sched = sched.scheduler(time.time, time.sleep)
    workload = args.workload
    result_file = args.csv_file
    
    csv_file = open(result_file, "w")
    writer = csv.DictWriter(csv_file, fieldnames=["batsim_id", "oar_id"])
    writer.writeheader()

    with open(workload, "r") as workload_file:
        json_data = json.load(workload_file)

        jobs = get_jobs(json_data)
        profiles = get_profiles(json_data)
        
        do_oarsub = lambda j: oar_command(j, profiles, writer, debug=args.debug)
    
        for job in get_jobs(json_data):
            oarsub_sched.enter(job["subtime"], 0, do_oarsub, kwargs={'j': job})
            
    oarsub_sched.run()
    
    csv_file.close()
    return 0

if __name__ == "__main__":
    main()
