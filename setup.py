from setuptools import setup

setup(
    # Application name:
    name="Lig",

    # Version number (initial):
    version="0.0",

    # Application author details:
    author="Quentin Guilloteau",
    author_email="Quentin.Guilloteau@inria.fr",

    # Packages
    packages=["app"],

    # Include additional files into the package
    # include_package_data=True,
    entry_points={
        'console_scripts': ['batsim_to_oarsub=app.batsim_to_oarsub:main'],
    },

    # Details
    url="https://gitlab.inria.fr/cigri-ctrl/batsim-workload-to-oarsub",

    # license="LICENSE.txt",
    description="Script executing oarsub commands based on a batsim workload",


    # Dependent packages (distributions)
    install_requires=[
    ]
)
